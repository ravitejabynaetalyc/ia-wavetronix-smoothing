# IA Wavetronix Smoothing

This project collects wavetronix data (in csv format) every 20 seconds and smoothens the data. Here, speed data is provided for each lane for a given detector. Hence, this speed data needs to be smoothened using Wavelet Transform.

## Getting Started

### Prerequisites
To succesfully run this project, you need JAVA 8, Maven.
Installing java on ubuntu: https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-debian-8

Installing maven on ubuntu: https://www.vultr.com/docs/how-to-install-apache-maven-on-ubuntu-16-04

### Installing

To run this project on your local machine:

1. git clone https://ravitejabynaetalyc@bitbucket.org/ravitejabynaetalyc/ia-wavetronix-smoothing.git
2. cd ia-wavetronix-smoothing
3. Go to applicaton.properties file in the project's root folder and edit "archive.root.folder=<local_data_store_path>" - the path where you want to store all the smoothing data. Example: "store.root.folder=/Users/ravitejarajabyna/etalyc/data"
3. Run "mvn clean install -DskipTests"
4. Open the project in IDE and Run/Debug Application.java for downloading data for smoothing data.
5. OR Run "java -jar target/ia-wavetronix-smoothing-1.0-SNAPSHOT.jar" downloading smoothing data. 
6. To confirm if the data is getting downloaded, go into the folder "local_data_store_path" and following is the folder structure for realtime and perday data storage: 
	Realtimedata(CSV format): <local_data_store_path>/iadot/smoothingdata/realtimedata/<data_source>.csv
	Realtimedata(XML format): <local_data_store_path>/iadot/smoothingdata/realtimedata/<data_source>.xml
	Perdaydata(CSV format): <local_data_store_path>/iadot/smoothingdata/perdatadata/<data_source>.csv

## Deployment

Currently, this data pipeline is running on aws ubuntu machines.

 - ssh -i "etalyc_camera.pem" ubuntu@ec2-34-229-228-181.compute-1.amazonaws.com

To install:
First Option:

1. Run the project as mentioned above locally and generate the jar files.
2. scp -i "/Users/ravitejarajabyna/Downloads/etalyc_camera.pem" target/ia-wavetronix-smoothing-1.0-SNAPSHOT.jar ubuntu@ec2-34-229-228-181.compute-1.amazonaws.com:data-smoothing.jar
3. ssh into the machine (ssh -i "etalyc_camera.pem" ubuntu@ec2-34-229-228-181.compute-1.amazonaws.com)
4. Kill the running jobs for "data-smoothing.jar"
5. Then run "nohup java -jar data-smoothing.jar &".

## Debugging
1. Check nohup.out for logs

## Functionality
1. Retrieve the wavetronix raw csv file from s3. Check application properties file for aws configuration details.
2. Apply Wavelet Transform Algorithm to the data and output csv file.
3. When the process is started for the first time, it takes 40 mins to generate the final csv file and it contiues to generate every 20 seconds.

	Input: etalyc-realtimedata/iarealtime/speed/speed.csv  (s3 bucket)
	Output: /home/ubuntu/data/iadot/smoothingdata/realtimedata/20190428.csv (local vm)


## Authors

* **Raviteja Raja Byna** - *Initial work* - Github (https://github.com/BRaviteja)

## License




