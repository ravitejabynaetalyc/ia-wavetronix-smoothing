package com.etalyc.mcomp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Stores configuration constant files from application.properties file.
 * 
 */
@Component
public class ConfigConstants {

	// Archive Folder Variables
	@Value("${archive.root.folder}")
	private String archiveRoot;
	@Value("${archive.iadot.root.folder}")
	private String dotArchiveRoot;

	public String getDotArchiveRealtimeFolder() {
		return dotArchiveRealtimeFolder;
	}

	public String getDotArchivePerdayFolder() {
		return dotArchivePerdayFolder;
	}

	@Value("${archive.iadot.realtimedata.folder}")
	private String dotArchiveRealtimeFolder;
	@Value("${archive.iadot.perdaydata.folder}")
	private String dotArchivePerdayFolder;

	@Value("${archive.iadot.smoothingdata.root.folder}")
	private String dotSmoothingArchiveRoot;

	@Value("${archive.iadot.smoothingdata.realtimedata.folder}")
	private String dotSmoothingArchiveRealtimeFolder;
	@Value("${archive.iadot.smoothingdata.perdaydata.folder}")
	private String dotSmoothingArchivePerdayFolder;

	public String getDotSmoothingArchiveRoot() {
		return dotSmoothingArchiveRoot;
	}

	public String getDotSmoothingArchiveRealtimeFolder() {
		return dotSmoothingArchiveRealtimeFolder;
	}

	public String getDotSmoothingArchivePerdayFolder() {
		return dotSmoothingArchivePerdayFolder;
	}

	public String getDecisionTreeModelPath() {
		return decisionTreeModelPath;
	}

	@Value("${archive.iadot.smoothingdata.model}")
	private String decisionTreeModelPath;

	public String getDotXmlPath() {
		return dotXmlPath;
	}

	@Value("${archive.iadot.xmlpath}")
	private String dotXmlPath;


	// Archive File Variables
	@Value("${archive.file.iadot.trafficData.name}")
	private String trafficDataFNPrefix;

	public String getIadotSmoothingdataPerdaydata() {
		return iadotSmoothingdataPerdaydata;
	}

	@Value("${iadot.smoothingdata.perdaydata}")
	private String iadotSmoothingdataPerdaydata;


	@Value("${aws_access_key_id}")
	private String awsAccessKeyId;

	@Value("${aws_secret_access_key}")
	private String awsSecretAccessKey;

	@Value("${aws_bucket_name}")
	private String awsBucketName;

	@Value("${aws_client_region}")
	private String awsClientRegion;

	public String getAwsAccessKeyId() {
		return awsAccessKeyId;
	}

	public String getAwsSecretAccessKey() {
		return awsSecretAccessKey;
	}

	public String getAwsBucketName() {
		return awsBucketName;
	}

	public String getAwsClientRegion() {
		return awsClientRegion;
	}

	public String getAwsRealtimedataBucketName() {
		return awsRealtimedataBucketName;
	}

	public String getArchiveRoot() {
		return archiveRoot;
	}

	public String getDotArchiveRoot() {
		return dotArchiveRoot;
	}

	public String getTrafficDataFNPrefix() {
		return trafficDataFNPrefix;
	}

	public String getAwsContainerIaRealtime() {
		return awsContainerIaRealtime;
	}

	@Value("${aws_container_ia_realtime}")
	private String awsContainerIaRealtime;

	@Value("${aws_realtimedata_bucket_name}")
	private String awsRealtimedataBucketName;






}
