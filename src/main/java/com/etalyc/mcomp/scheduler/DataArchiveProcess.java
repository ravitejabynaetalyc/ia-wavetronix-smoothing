package com.etalyc.mcomp.scheduler;

import com.etalyc.mcomp.config.ConfigConstants;
import com.etalyc.mcomp.smoothing.TIMELIPipeLineSmoothing;
import com.etalyc.mcomp.smoothing.TIMELISensorClass;
import com.etalyc.mcomp.utils.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Component
public class DataArchiveProcess {

    @Autowired
    ConfigConstants configConstants;

    @Autowired
    GetObject getObject;

    private static HashMap<String, TIMELISensorClass> IWZClassMap;

    private static String todayFile = "";
    ArrayList<String> sensorIDs = new ArrayList<>();
//    private static String xmlPath = "/Users/ravitejarajabyna/ra/datasource/datadrive2/iadot/trafficdata.xml";


    /**
     * Archives Traffic Detector Data into a file as CSV format.
     *
     * @throws Exception
     */
    public void archiveTrafficDetectorData() throws Exception {
        String xmlPath = configConstants.getDotXmlPath();
        String xmlContent = getObject.getRealtimeDataFromAWS(configConstants.getAwsContainerIaRealtime(), configConstants.getTrafficDataFNPrefix() + FileUtils.FileType.XML.getExtType(), configConstants.getTrafficDataFNPrefix(), configConstants.getAwsRealtimedataBucketName());

        File internalFile = FileUtils.archiveDataForInternal(configConstants.getDotArchiveRoot(),
                configConstants.getTrafficDataFNPrefix(), xmlContent);

        editMalformedXml(internalFile.getAbsolutePath());
        long startTime = System.currentTimeMillis();
        System.out.println("Start smoothing : " + new Date());
        Object [] historyObjs = TIMELIPipeLineSmoothing.smoothing(xmlPath, IWZClassMap, todayFile, sensorIDs, configConstants.getDotSmoothingArchiveRealtimeFolder(), configConstants.getDotSmoothingArchivePerdayFolder(), configConstants.getDecisionTreeModelPath(), Boolean.valueOf(configConstants.getIadotSmoothingdataPerdaydata()));
        System.out.println("End smoothing : " + new Date());
        System.out.println("Total time : " + (startTime - System.currentTimeMillis()));
        IWZClassMap = (HashMap<String, TIMELISensorClass>)historyObjs[0];
        sensorIDs   = (ArrayList<String>)historyObjs[1];

        String today = new SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
        todayFile = today;

    }

    private static void editMalformedXml(String fileName) throws IOException {
        List<String> newLines = new ArrayList<>();
        for (String line : Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8)) {
            line = line.replace("<String>", "").replace("</String>", "");
            line = StringEscapeUtils.unescapeXml(line);
            if (line.contains(",")) {
                newLines.add(line.replace(",", " "));
            } else if (line.contains("& ")){
                newLines.add(line.replace("& ", "&amp;"));
            } else {
                newLines.add(line);
            }
        }
        Files.write(Paths.get(fileName), newLines, StandardCharsets.UTF_8);
    }
}
