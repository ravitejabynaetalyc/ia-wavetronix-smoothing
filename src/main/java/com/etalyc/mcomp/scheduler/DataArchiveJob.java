package com.etalyc.mcomp.scheduler;

import com.etalyc.mcomp.handlers.AsyncCallBack;
import com.etalyc.mcomp.handlers.Callback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Schedular job for data archiving.
 *
 */
@ConditionalOnProperty(value = "data.scheduling.enable", havingValue = "true", matchIfMissing = false)
@Component
public class DataArchiveJob {

	@Autowired
	private DataArchiveProcess dataArchiveProcess;

	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@PostConstruct
	public void runOnStartup() {
		executeArchiveTrafficData();
//		System.out.println(externalConfig.getEnableWavetronix());
//		if (externalConfig.getEnableWavetronix()) {
//			//executeArchiveTrafficData();
//		}//Fetch Wavetronics
//		//executeArchiveSensorInventoryData();
//		// executeArchiveTrafficIncidentData();
//		// executeArchiveRealTimeFlowData();
////		if (externalConfig.getEnableWaze()) executeArchiveWazeFeed();
//		System.out.println(externalConfig.getEnableInrix());
//		if (externalConfig.getEnableInrix()) executeArchiveInrixFeed();// Fetch Inrix
//		if (externalConfig.getEnableCamera()) executeArchiveCameraInventory();
//		if (externalConfig.getEnableDMS()) executeArchiveDMSInventory();
//		if (externalConfig.getEnableWorkzone()) executeArchiveWorkzoneInventory();
	}

	/**
	 * Job to generate Traffic Date archiving.
	 *
	 * @throws Exception
	 */
	@Scheduled(cron = "${job.schedule.trafficDetectorData.cron}")
	public void executeArchiveTrafficData() {
		new AsyncCallBack(new Callback() {

			@Override
			public void call() {
				try {
					// System.out.println("Starting detector archive job at :" +
					// DateUtils.getCurrentDate());
					dataArchiveProcess.archiveTrafficDetectorData();
					// System.out.println("Completed detector archive job at :"
					// + DateUtils.getCurrentDate());
				} catch (Exception e) {
					// TODO Add logging
					e.printStackTrace();
				}

			}
		}).execute(taskExecutor);

	}


}
